# Rounded Corners Stencil

This stencil shall help you to create beautiful rounded corners on your sewing project.
Print it out and glue it on a thick cardboard or visit your friendly Hackspace/ Makerspace 
to laser cut your plexiglass version.

## License
CC0
